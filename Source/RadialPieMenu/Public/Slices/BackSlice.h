// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Slice.h"
#include "Components/Button.h"

#include "BackSlice.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Components))
class RADIALPIEMENU_API UBackSlice : public USlice
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UBackSlice();
	
protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void OnClicked(UPrimitiveComponent * Component, FKey ButtonPressed) override;


	UPROPERTY() UButton *BackButton;

	UFUNCTION()
	void OnBackButtonPressed();

private:

};
