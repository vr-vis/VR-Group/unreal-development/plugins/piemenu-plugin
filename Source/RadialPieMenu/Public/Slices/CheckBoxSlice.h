// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Slice.h"
#include "Components/CheckBox.h"

#include "CheckBoxSlice.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Components), meta = (BlueprintSpawnableComponent))
class RADIALPIEMENU_API UCheckBoxSlice : public USlice
{
	GENERATED_BODY()
	
public:
	// Sets default values for this component's properties
	UCheckBoxSlice();

	bool GetCheckBoxState();

	DECLARE_DELEGATE_OneParam(FCheckBoxStateChanged, bool);
	FCheckBoxStateChanged OnStateChanged;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	virtual void OnClicked(UPrimitiveComponent * Component, FKey ButtonPressed) override;

	UPROPERTY() UCheckBox *CheckBox;

	bool CheckBoxState, OldCheckBoxState;

	UFUNCTION()
	void OnCheckStateChanged(bool InState);

private:

};
