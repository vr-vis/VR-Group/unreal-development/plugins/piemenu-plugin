// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Slice.h"
#include "Components/Slider.h"

#include "SliderSlice.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Components), meta = (BlueprintSpawnableComponent))
class RADIALPIEMENU_API USliderSlice : public USlice
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	USliderSlice();

	UFUNCTION(BlueprintCallable)
	float GetSliderValue() const;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	virtual void OnClicked(UPrimitiveComponent * Component, FKey ButtonPressed) override;

	UPROPERTY() USlider *Slider;

	UPROPERTY(EditAnywhere)
	float MinValue;

	UPROPERTY(EditAnywhere)
	float MaxValue;

	float SliderValue;

	UFUNCTION()
	void OnValueChanged(float InValue);

private:

};
