// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Slice.h"
#include "Components/CheckBox.h"
#include "Components/HorizontalBox.h"
#include "RadioButtonSlice.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Components), meta = (BlueprintSpawnableComponent))
class RADIALPIEMENU_API URadioButtonSlice : public USlice
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	URadioButtonSlice();

	bool GetRadioButtonState();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	virtual void OnClicked(UPrimitiveComponent * Component, FKey ButtonPressed) override;

	UCheckBox *RadioButton;

	UHorizontalBox *HorizontalBox;
	bool RadioButtonState, OldRadioButtonState;

	UFUNCTION()
	void OnRadioButtonStateChanged(bool InState);

private:
	TArray<URadioButtonSlice*> BrethrenRadioButtons;
	bool IsInitialized;
};
