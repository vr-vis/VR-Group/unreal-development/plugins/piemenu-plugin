// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "DisplayClusterEventWrapper.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EventHandlerActor.generated.h"

UCLASS()
class RADIALPIEMENU_API AEventHandlerActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEventHandlerActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FPieMenuNotifyDelegate, FString, SenderName);

	UPROPERTY(BlueprintAssignable)
	FPieMenuNotifyDelegate EventNotifier;

	void ClusterEventSender(FString Name);
	DECLARE_DISPLAY_CLUSTER_EVENT(AEventHandlerActor, ClusterEventSender);
};
