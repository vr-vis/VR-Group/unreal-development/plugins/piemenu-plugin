// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Common.h"
#include "CoreMinimal.h"
#include "Engine/World.h"
#include "Engine/Engine.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "Components/WidgetComponent.h"
#include "GameFramework/PlayerController.h"
#include "Materials/MaterialInstanceDynamic.h"

#include "EventHandlerActor.h"
#include "WidgetTree.h"
#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Kismet/GameplayStatics.h"
#include "Components/WidgetComponent.h"
#include "UObject/ConstructorHelpers.h"

#include "Slice.generated.h"

class UMainMenu;
class USubMenu;

UCLASS( ClassGroup=(Components) )
class RADIALPIEMENU_API USlice : public UWidgetComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USlice();
	
	UPROPERTY() AEventHandlerActor* EventHandlerActor;
	UPROPERTY() UProceduralMeshComponent *ProceduralMesh;

	TEnumAsByte<EMenuSliceTypes> SliceType;

	float Opacity;

	UPROPERTY(EditAnywhere)
	FLinearColor Color;

	void InitializeSliceParameters();

	//makes submenus visible and makes "background" menus transparent (I think...)

	//REFACTORING===============================
	void SetCollision();
	void GenerateSliceMesh(int place, float segmentOffset, UMaterial* material);
	void changeWidgetVisibility();
	void scaleSliceOpacityAndColor(float factor);

	int sliceLevel;
	//==========================================

	UPROPERTY(EditAnywhere)
	int Percentage;

	//UFUNCTION()
	//void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	//UFUNCTION()
	//void OnOverlapEnd(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
	virtual void OnClicked(UPrimitiveComponent * Component, FKey ButtonPressed);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
	//==================from refactoring, pulled out of subclasses ================================
	
	UUserWidget *UserWidget;	//change to non-capitalized later....
	TSubclassOf<UUserWidget> WidgetClass;
	
public:
	float sliceOffset;
	float degree;	//degree of the circle segment

	//======================================================
	TArray<FVector>Vertices;
	TArray<int32>Triangles;
	TArray<FVector>Normals;
	TArray<FProcMeshTangent>Tangents;
	TArray<FVector2D>UVs;
	TArray<FLinearColor>Colors;

	int32 QuadIndexCount;
	FVector DefinedShape[8];

	UMaterialInstanceDynamic *DynamicMaterial;

	float DistanceToOtherActor, DistanceToActor;
	
	UPROPERTY(EditAnywhere)
	float Width;

	UPROPERTY(EditAnywhere)
	float Height;
};
