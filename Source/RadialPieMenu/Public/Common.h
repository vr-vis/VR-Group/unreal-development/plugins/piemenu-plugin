// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/World.h"
#include "Engine/Engine.h"
#include "Components/WidgetComponent.h"

//Forward Declarations
class UMenu;
class USlice;

//Global variables
static UMenu* mainMenu = nullptr;
static int currentMenuLevel = 0;
static float const innerRadius = 15.0f;
static float const outerRadius = 60.0f;
static float const depth = 5.0f;

static float const backslice_radius = 7.5f;

UENUM()
enum EMenuSliceTypes
{
	TYPE_CheckBox				UMETA(DisplayName = "Check Box"),
	TYPE_RadioButton			UMETA(DisplayName = "Radio Button"),
	TYPE_Slider					UMETA(DisplayName = "Slider"),
	TYPE_ColorPicker			UMETA(DisplayName = "Color Picker"),
	TYPE_ColorMapEditor			UMETA(DisplayName = "Color Map Editor"),
	TYPE_ColorChoosingMenu		UMETA(DisplayName = "Color Choosing Menu"),
	TYPE_Back					UMETA(DisplayName = "Back"),
	TYPE_SubMenu				UMETA(DisplayName = "Submenu")
};

//into class slice
void SetWidgetPosition(UWidgetComponent *WidgetComponent, float StartAlpha, float OuterRadius, float InnerRadius, float Depth, float Width, float Height, int PieDegree);
