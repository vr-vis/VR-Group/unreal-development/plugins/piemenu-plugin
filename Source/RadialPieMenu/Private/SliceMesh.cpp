// Fill out your copyright notice in the Description page of Project Settings.

#include "SliceMesh.h"
#include "Slice.h"
#include "Common.h"

SliceMesh::SliceMesh()
{
}

SliceMesh::~SliceMesh()
{
}

void SliceMesh::GenerateSliceMesh(USlice * slice, int place, float segmentOffset, UMaterial * material)
{
	
	//back slices are full circles
	if (slice->SliceType == EMenuSliceTypes::TYPE_Back)
	{
		slice->sliceOffset = 0;
		slice->degree = 360;
	}
	else
	{
		slice->sliceOffset = segmentOffset;
		slice->degree = FMath::FloorToInt(360 * slice->Percentage / 100);
	}
	
	for (int j = 0; j < slice->degree; j++)
	{
		float beginOfSegment = FMath::DegreesToRadians(slice->sliceOffset + (j * 1.f));
		float endOfSegment = FMath::DegreesToRadians(slice->sliceOffset + ((j + 1) * 1.f));

		if (j == 0)
		{
			GenerateMesh(slice, beginOfSegment, endOfSegment, true, false);
		}
		else if (j == slice->degree - 1)
		{
			GenerateMesh(slice, beginOfSegment, endOfSegment, false, true);
		}
		else
		{
			GenerateMesh(slice, beginOfSegment, endOfSegment, false, false);
		}
	}

	FString TempMeshName = TEXT("Mesh" + FString::FromInt(place));
	slice->ProceduralMesh = NewObject<UProceduralMeshComponent>(slice, FName(*TempMeshName));

	FString CollisionMeshName = TEXT("CollisionMesh" + FString::FromInt(place));
	slice->ProceduralMesh->SetCollisionProfileName(FName(*CollisionMeshName));

	FString CompName = TEXT("Comp" + FString::FromInt(place));
	FString MatInstDynamicName = TEXT("MatInstDynamic" + FString::FromInt(place));

	slice->DynamicMaterial = UMaterialInstanceDynamic::Create(material, slice->ProceduralMesh);
	slice->DynamicMaterial->SetScalarParameterValue(FName(TEXT("Opacity")), slice->Opacity);
	slice->DynamicMaterial->SetVectorParameterValue(FName(TEXT("Color")), slice->Color);
	slice->ProceduralMesh->SetMaterial(0, slice->DynamicMaterial);

	slice->ProceduralMesh->CreateMeshSection_LinearColor(0, slice->Vertices, slice->Triangles, slice->Normals, slice->UVs, slice->Colors, slice->Tangents, true); //true: bCreateCollision, 0: sectionIndex
	//Generation of mesh finished
}

void SliceMesh::GenerateMesh(USlice* slice, float BegAlphaInFunction, float EndAlphaInFunction, bool Bottom, bool Top)
{
	float MidAlpha = (BegAlphaInFunction + EndAlphaInFunction) / 2.f;

	if (slice->SliceType == EMenuSliceTypes::TYPE_Back)
	{
		slice->DefinedShape[0] = FVector(0.f, -backslice_radius * FMath::Cos(EndAlphaInFunction), backslice_radius*FMath::Sin(EndAlphaInFunction));
		slice->DefinedShape[1] = FVector(0.f, 0.0f, 0.0f);
		slice->DefinedShape[2] = FVector(0.f, -backslice_radius * FMath::Cos(BegAlphaInFunction), backslice_radius*FMath::Sin(BegAlphaInFunction));
		slice->DefinedShape[3] = FVector(0.f, 0.0f, 0.0f);

		slice->DefinedShape[4] = FVector(depth, -backslice_radius * FMath::Cos(EndAlphaInFunction), backslice_radius*FMath::Sin(EndAlphaInFunction));
		slice->DefinedShape[5] = FVector(depth, 0.0f, 0.0f);
		slice->DefinedShape[6] = FVector(depth, -backslice_radius * FMath::Cos(BegAlphaInFunction), backslice_radius*FMath::Sin(BegAlphaInFunction));
		slice->DefinedShape[7] = FVector(depth, 0.0f, 0.0f);
	}
	else
	{
		slice->DefinedShape[0] = FVector(0.f, -outerRadius * FMath::Cos(EndAlphaInFunction), outerRadius*FMath::Sin(EndAlphaInFunction)); // Forward Top Left
		slice->DefinedShape[1] = FVector(0.f, -innerRadius * FMath::Cos(EndAlphaInFunction), innerRadius*FMath::Sin(EndAlphaInFunction)); // Forward Top Right
		slice->DefinedShape[2] = FVector(0.f, -outerRadius * FMath::Cos(BegAlphaInFunction), outerRadius*FMath::Sin(BegAlphaInFunction)); // Forward Bottom Left
		slice->DefinedShape[3] = FVector(0.f, -innerRadius * FMath::Cos(BegAlphaInFunction), innerRadius*FMath::Sin(BegAlphaInFunction)); // Forward Bottom Right

		slice->DefinedShape[4] = FVector(depth, -outerRadius * FMath::Cos(EndAlphaInFunction), outerRadius*FMath::Sin(EndAlphaInFunction)); // Backward Top Left
		slice->DefinedShape[5] = FVector(depth, -innerRadius * FMath::Cos(EndAlphaInFunction), innerRadius*FMath::Sin(EndAlphaInFunction)); // Backward Top Right
		slice->DefinedShape[6] = FVector(depth, -outerRadius * FMath::Cos(BegAlphaInFunction), outerRadius*FMath::Sin(BegAlphaInFunction)); // Backward Bottom Left
		slice->DefinedShape[7] = FVector(depth, -innerRadius * FMath::Cos(BegAlphaInFunction), innerRadius*FMath::Sin(BegAlphaInFunction)); // Backward Bottom Right
	}
	// Front
	FProcMeshTangent TangentSetup = FProcMeshTangent(1.f, 0.f, 0.f);
	AddQuadMesh(slice, slice->DefinedShape[0], slice->DefinedShape[2], slice->DefinedShape[1], slice->DefinedShape[3], slice->QuadIndexCount, TangentSetup);

	// Back
	TangentSetup = FProcMeshTangent(-1.f, 0.f, 0.f);
	AddQuadMesh(slice, slice->DefinedShape[5], slice->DefinedShape[7], slice->DefinedShape[4], slice->DefinedShape[6], slice->QuadIndexCount, TangentSetup);

	// Left
	TangentSetup = FProcMeshTangent(0.f, -FMath::Cos(MidAlpha), FMath::Sin(MidAlpha));
	AddQuadMesh(slice, slice->DefinedShape[4], slice->DefinedShape[6], slice->DefinedShape[0], slice->DefinedShape[2], slice->QuadIndexCount, TangentSetup);

	// Right
	TangentSetup = FProcMeshTangent(0.f, FMath::Cos(MidAlpha), -FMath::Sin(MidAlpha));
	AddQuadMesh(slice, slice->DefinedShape[1], slice->DefinedShape[3], slice->DefinedShape[5], slice->DefinedShape[7], slice->QuadIndexCount, TangentSetup);

	// Bottom
	if (Bottom)
	{
		TangentSetup = FProcMeshTangent(0.f, -FMath::Sin(BegAlphaInFunction), -FMath::Cos(BegAlphaInFunction));
		AddQuadMesh(slice, slice->DefinedShape[7], slice->DefinedShape[3], slice->DefinedShape[6], slice->DefinedShape[2], slice->QuadIndexCount, TangentSetup);
	}

	// Top
	if (Top)
	{
		TangentSetup = FProcMeshTangent(0.f, FMath::Sin(EndAlphaInFunction), FMath::Cos(EndAlphaInFunction));
		AddQuadMesh(slice, slice->DefinedShape[1], slice->DefinedShape[5], slice->DefinedShape[0], slice->DefinedShape[4], slice->QuadIndexCount, TangentSetup);
	}
}

void SliceMesh::AddQuadMesh(USlice* slice, FVector TopRight, FVector BottomRight, FVector TopLeft, FVector BottomLeft, int32& QuadIndex, FProcMeshTangent Tangent)
{
	int32 Point1 = QuadIndex++;
	int32 Point2 = QuadIndex++;
	int32 Point3 = QuadIndex++;
	int32 Point4 = QuadIndex++;

	slice->Vertices.Add(TopRight);
	slice->Vertices.Add(BottomRight);
	slice->Vertices.Add(TopLeft);
	slice->Vertices.Add(BottomLeft);

	slice->Triangles.Add(Point1);
	slice->Triangles.Add(Point2);
	slice->Triangles.Add(Point3);

	slice->Triangles.Add(Point4);
	slice->Triangles.Add(Point3);
	slice->Triangles.Add(Point2);

	FVector ThisNorm = FVector::CrossProduct(TopLeft - BottomRight, TopLeft - TopRight).GetSafeNormal();
	for (int i = 0; i < 4; i++)
	{
		slice->Normals.Add(ThisNorm);
		slice->Tangents.Add(Tangent);
		slice->Colors.Add(FLinearColor::Green);
	}

	slice->UVs.Add(FVector2D(0.f, 1.f)); //Top Left
	slice->UVs.Add(FVector2D(0.f, 0.f)); //Bottom Left
	slice->UVs.Add(FVector2D(1.f, 1.f)); //Bottom Right
	slice->UVs.Add(FVector2D(1.f, 0.f)); //Bottom Right
}

