// Fill out your copyright notice in the Description page of Project Settings.


#include "Slices/RadioButtonSlice.h"

#include "Menu.h"

// Sets default values for this component's 
URadioButtonSlice::URadioButtonSlice()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	// PrimaryComponentTick.bCanEverTick = true;

	Width = 22.f;
	Height = 10.f;

	IsInitialized = false;

	this->SliceType = EMenuSliceTypes::TYPE_RadioButton;

	//TODO : Will be changed
	static ConstructorHelpers::FClassFinder<UUserWidget> WidgetHolder(TEXT("/RadialPieMenu/SliceEquipments/RadioButtonWidget"));

	if (WidgetHolder.Succeeded())
	{
		WidgetClass = WidgetHolder.Class;
	}
	else
	{
		WidgetClass = nullptr;
	}
}

// Called when the game 
void URadioButtonSlice::BeginPlay()
{
	Super::BeginPlay();

	UserWidget = CreateWidget<UUserWidget>(GetWorld(), WidgetClass);

	this->SetWidget(UserWidget);
	this->SetDrawSize(FVector2D(Width, Height));

	SetWidgetPosition(this, sliceOffset, outerRadius, innerRadius, depth, Width, Height, degree);

	RadioButton = (UCheckBox*)UserWidget->WidgetTree->FindWidget(FName("RadioButton"));

	if (RadioButton)
	{
		RadioButton->OnCheckStateChanged.AddDynamic(this, &URadioButtonSlice::OnRadioButtonStateChanged);
	}
}

// Called every frame
void URadioButtonSlice::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void URadioButtonSlice::OnClicked(UPrimitiveComponent * Component, FKey ButtonPressed)
{
	Super::OnClicked(Component, ButtonPressed);

	if (OldRadioButtonState != RadioButtonState)
	{
		for(URadioButtonSlice* Radio : BrethrenRadioButtons)
		{
			Radio->RadioButtonState = false;
			Radio->RadioButton->SetCheckedState(ECheckBoxState::Unchecked);
		}
	}

	OldRadioButtonState = RadioButtonState;
}

bool URadioButtonSlice::GetRadioButtonState()
{
	return RadioButtonState;
}

void URadioButtonSlice::OnRadioButtonStateChanged(bool InState)
{
	RadioButtonState = true;
	FString RadioButtonName = this->GetName();
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, *RadioButtonName);
	
	UMenu* ParentMenu = mainMenu->findParentMenuOfSlice(this);

	for (USlice* Slice : ParentMenu->GetPieSlices())
	{
		if (Slice->SliceType == EMenuSliceTypes::TYPE_RadioButton)
		{
			if (Slice == this)
			{
				continue;
			}

			URadioButtonSlice* Radio = Cast<URadioButtonSlice>(Slice);
			Radio->OldRadioButtonState = RadioButtonState;
			Radio->RadioButtonState = true;
			Radio->RadioButton->SetCheckedState(ECheckBoxState::Unchecked);
		}
	}

	IsInitialized = true;
	
	RadioButton->SetCheckedState(ECheckBoxState::Checked);
	this->OnClicked(this, EKeys::LeftMouseButton.GetFName());
}