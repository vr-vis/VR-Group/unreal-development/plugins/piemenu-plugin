// Fill out your copyright notice in the Description page of Project Settings.


#include "ColorPickerSlice.h"

// Sets default values for this component's 
UColorPickerSlice::UColorPickerSlice()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	// PrimaryComponentTick.bCanEverTick = true;

	Width = 20.f;
	Height = 20.f;

	this->SliceType = EMenuSliceTypes::TYPE_ColorPicker;
	static ConstructorHelpers::FClassFinder<UUserWidget> WidgetHolder(TEXT("/RadialPieMenu/SliceEquipments/ColorPickerWidget"));

	if (WidgetHolder.Succeeded())
	{
		WidgetClass = WidgetHolder.Class;
	}
	else
	{
		WidgetClass = nullptr;
	}
}

// Called when the game 
void UColorPickerSlice::BeginPlay()
{
	Super::BeginPlay();

	UserWidget = CreateWidget<UUserWidget>(GetWorld(), WidgetClass);

	this->SetWidget(UserWidget);
	this->SetDrawSize(FVector2D(Width, Height));

	SetWidgetPosition(this, sliceOffset, outerRadius, innerRadius, depth, Width, Height, degree);
	
	ColorMap = (UButton*)UserWidget->WidgetTree->FindWidget(FName("Image"));
	CanvasPanel = (UCanvasPanel*)UserWidget->WidgetTree->FindWidget(FName("CanvasPanelPicker"));
	if (ColorMap && CanvasPanel)
	{
		ColorMap->OnPressed.AddDynamic(this, &UColorPickerSlice::OnMouseClicked);
	}
}

// Called every frame
void UColorPickerSlice::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

}

void UColorPickerSlice::OnClicked(UPrimitiveComponent * Component, FKey ButtonPressed)
{
	Super::OnClicked(Component, ButtonPressed);

	FString ColorString = ChosenColor.ToString();
	GEngine->AddOnScreenDebugMessage(1, 2.f, FColor::Red, "CheckBox State Changed To " + ColorString);
}

FLinearColor UColorPickerSlice::GetColor()
{
	return ChosenColor;
}

void UColorPickerSlice::OnMouseClicked()
{
	//this has to be changed to open a color picker widget later

	float mousePosX, mousePosY;
	FVector2D scaledPosition;
	GetWorld()->GetFirstPlayerController()->GetMousePosition(mousePosX, mousePosY);
	FVector2D mousePosition = FVector2D(mousePosX, mousePosY);
	FVector widgetLocation = this->GetComponentLocation();

	GetWorld()->GetFirstPlayerController()->ProjectWorldLocationToScreen(widgetLocation, scaledPosition);
	FVector2D difPosition = mousePosition - scaledPosition;	
	float localDegree = FMath::Atan2(difPosition.Y, difPosition.X) * 180 / PI;
	float clampedHue = (localDegree < 0) ? -localDegree : 360.f - localDegree;
	clampedHue = (float)((int)(clampedHue + 270.f) % 360);
	float clampedSaturation = 2 * difPosition.Size() / Width;	//Size() is vector length
	FLinearColor chosenColor = FLinearColor(clampedHue, clampedSaturation, 100.f, 1.f).HSVToLinearRGB();
	DynamicMaterial->SetVectorParameterValue(FName(TEXT("Color")), chosenColor);

	ChosenColor = chosenColor;
	this->OnClicked(this, EKeys::LeftMouseButton.GetFName());
}