// Fill out your copyright notice in the Description page of Project Settings.


#include "Slices/SliderSlice.h"

// Sets default values for this component's 
USliderSlice::USliderSlice()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	// PrimaryComponentTick.bCanEverTick = true;

	MinValue = 0.f;
	MaxValue = 100.f;

	Width = 30.f;
	Height = outerRadius - innerRadius + 10.0f;

	this->SliceType = EMenuSliceTypes::TYPE_Slider;

	static ConstructorHelpers::FClassFinder<UUserWidget> WidgetHolder(TEXT("/RadialPieMenu/SliceEquipments/SliderWidget"));

	if (WidgetHolder.Succeeded())
	{
		WidgetClass = WidgetHolder.Class;
	}
	else
	{
		WidgetClass = nullptr;
	}
	//TODO : Will be changed
}

void USliderSlice::BeginPlay()
{
	Super::BeginPlay();

	UserWidget = CreateWidget<UUserWidget>(GetWorld(), WidgetClass);

	this->SetWidget(UserWidget);
	this->SetDrawSize(FVector2D(Width, Height));

	SetWidgetPosition(this, sliceOffset, outerRadius, innerRadius, depth, Width, Height, degree);

	Slider = Cast<USlider>(UserWidget->WidgetTree->FindWidget(FName("Slider")));

	if (Slider)
	{
		Slider->OnValueChanged.AddDynamic(this, &USliderSlice::OnValueChanged);
	}

	changeWidgetVisibility();
}

// Called every frame
void USliderSlice::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

}

void USliderSlice::OnClicked(UPrimitiveComponent * Component, FKey ButtonPressed)
{
	Super::OnClicked(Component, ButtonPressed);

	
}

float USliderSlice::GetSliderValue() const
{
	return SliderValue;
}

void USliderSlice::OnValueChanged(float InValue)
{
	SliderValue = (MinValue + (MaxValue - MinValue) * Slider->GetValue());

	this->OnClicked(this, EKeys::LeftMouseButton.GetFName());
}
