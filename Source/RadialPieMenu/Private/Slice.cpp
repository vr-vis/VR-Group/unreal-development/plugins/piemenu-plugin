// Fill out your copyright notice in the Description page of Project Settings.

#include "Slice.h"
#include "Menu.h"
#include "SliceMesh.h"
#include "EngineUtils.h"
#include "Kismet/GameplayStatics.h"

// Sets default values for this component's properties
USlice::USlice()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame. You can turn these features
	// off to improve performance if you don't need them.
	// PrimaryComponentTick.bCanEverTick = true;

	InitializeSliceParameters();
}

// Called when the game starts
void USlice::BeginPlay()
{
	Super::BeginPlay();

	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AEventHandlerActor::StaticClass(), FoundActors);

	if(FoundActors.Num() > 0 && !EventHandlerActor)
	{
		EventHandlerActor = Cast <AEventHandlerActor>(FoundActors[0]);
	}
	else
	{
		const FVector SpawnLocation = FVector(0.0f, 0.0f, 0.0f);
		const FRotator SpawnRotation = FRotator(0.0f, 0.0f, 0.0f);
		const FActorSpawnParameters SpawnInfo;
		EventHandlerActor = GetWorld()->SpawnActor<AEventHandlerActor>(AEventHandlerActor::StaticClass(), SpawnLocation, SpawnRotation, SpawnInfo);
	}


}

// Called every frame
void USlice::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	changeWidgetVisibility();
}

void USlice::InitializeSliceParameters()
{
	QuadIndexCount = 0;

	Color = FColor::White;
	Opacity = 1.f;	
}

// TODO: after adding a submenu slice, this event handler won't be necessary anymore
void USlice::OnClicked(UPrimitiveComponent * Component, FKey ButtonPressed)
{
	if (!mainMenu)
		return;

	//Send cluster event. TODO: Definitely check whether this works in n-display environments!
	EventHandlerActor->ClusterEventSender(this->GetName());
	
	//TODO: This function will be moved into a SubMenuSlice later on, but for testing it's fine I guess...
	//new menu structure is used
	
	UMenu* currentOpenMenu = mainMenu->findParentMenuOfSlice(this);

	if ((currentMenuLevel == currentOpenMenu->menuLevel))
	{
		if (this->SliceType == EMenuSliceTypes::TYPE_Back)
		{
			currentMenuLevel--;
			currentOpenMenu->CloseMenu();
		}
		else
		{
			if (currentOpenMenu->subMenus.Contains(this))
			{
				currentMenuLevel++;
				currentOpenMenu->subMenus[this]->OpenMenu();
			}
		}

		mainMenu->SetMenuCollision();
	}
}

void USlice::SetCollision()
{
	if (this->sliceLevel == currentMenuLevel || currentMenuLevel == 0)
	{
		ProceduralMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		ProceduralMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	}
	else
	{
		ProceduralMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		ProceduralMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	}
}

void USlice::GenerateSliceMesh(int place, float segmentOffset, UMaterial* material)
{
	
	SliceMesh::GenerateSliceMesh(this, place, segmentOffset, material);

	UMenu* parentMenu = mainMenu->findParentMenuOfSlice(this);
	
	SetCollision();
	ProceduralMesh->OnClicked.AddDynamic(this, &USlice::OnClicked);
	ProceduralMesh->AttachToComponent(parentMenu, FAttachmentTransformRules::SnapToTargetIncludingScale, "AttachSocketName");

	if (parentMenu != mainMenu)
	{
		//make submenus invisible at first
		ProceduralMesh->SetVisibility(false);
	}

	ProceduralMesh->RegisterComponent();
}

void USlice::changeWidgetVisibility()
{
	if (UserWidget)
	{
		if (ProceduralMesh && ProceduralMesh->IsVisible())
		{
			UserWidget->SetVisibility(ESlateVisibility::Visible);
			UserWidget->SetIsEnabled(true);
			
			//needs a change later	
			
			if (this->SliceType == EMenuSliceTypes::TYPE_Back)
			{
				SetWidgetPosition(this, sliceOffset, backslice_radius, 0.0f, depth, Width, Height, degree);
			}
			else
			{
				SetWidgetPosition(this, sliceOffset, outerRadius, innerRadius, depth, Width, Height, degree);
			}
		}
		else
		{
			UserWidget->SetVisibility(ESlateVisibility::Collapsed);	//collapsed doesn't use layout space
			UserWidget->SetIsEnabled(false);
			//note: does it really matter, where the widget is drawn, if it is invisible?
			//SetWidgetPosition(this, sliceOffset, outerRadius, innerRadius, depth - 1000.0f, Width, Height, degree);	//1000.0f : WidgetOutDepth
			
		}
	}
}

//currently unused, but might be used later
void USlice::scaleSliceOpacityAndColor(float factor)
{
	Color *= factor;
	Opacity *= factor;

	DynamicMaterial->SetVectorParameterValue(FName(TEXT("Color")), Color);
	DynamicMaterial->SetScalarParameterValue(FName(TEXT("Opacity")), Opacity);

	SetCollision();
}


//END REFACTORING ======================================================================================================================================
