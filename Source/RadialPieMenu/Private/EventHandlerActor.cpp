// Fill out your copyright notice in the Description page of Project Settings.


#include "EventHandlerActor.h"

// Sets default values
AEventHandlerActor::AEventHandlerActor()
{
 	// Set this actor to call Tick() every frame. You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AEventHandlerActor::BeginPlay()
{
	Super::BeginPlay();
}

void AEventHandlerActor::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	EventNotifier.Clear();
	Super::EndPlay(EndPlayReason);
}

// Called every frame
void AEventHandlerActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AEventHandlerActor::ClusterEventSender(FString Name)
{
	EventNotifier.Broadcast(Name);
}
