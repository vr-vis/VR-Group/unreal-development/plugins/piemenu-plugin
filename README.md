# Pie Menus

This plugin integrats the pie menus known from ViStA back into Unreal. The paper this is based on https://ieeexplore.ieee.org/document/6479193.

# Setup
```bash
git submodule init
git submodule add -b master https://devhub.vr.rwth-aachen.de/VR-Group/unreal-development/piemenu-plugin.git Plugins/RadialPieMenu
```

## Add Action Bindings in Project:
1. Add “Click” named Action Mapping into Project Settings
2. Add following code lines into Pawn header:
```cpp
#include "Components/WidgetInteractionComponent.h"

/*
* Widget Interaction Related Properties
*/
UPROPERTY(EditAnywhere)
UWidgetInteractionComponent *WidgetInteractionComponent;

UFUNCTION(BlueprintCallable)
void Pressed();

UFUNCTION(BlueprintCallable)
void Released();
```
3. Add following code lines into Pawn cpp file:
```cpp
void BeginPlay[...]{
	// Create Widget Interaction Component
	WidgetInteractionComponent = NewObject<UWidgetInteractionComponent>(this, FName("WidgetInteractionComponent"));
	WidgetInteractionComponent->InteractionSource = EWidgetInteractionSource::Mouse; //Or Custom
	WidgetInteractionComponent->DebugColor = FColor::Red;
	WidgetInteractionComponent->bShowDebug = true;
	WidgetInteractionComponent->RegisterComponent();
}
```
```cpp
void SetupPlayerInputComponent[...]{
	// Detect press and release
	PlayerInputComponent->BindAction("Click", IE_Pressed, this,
	&ARadialMenuCharacter::Pressed);

	PlayerInputComponent->BindAction("Click", IE_Released, this,
	&ARadialMenuCharacter::Released);
}
```
```cpp
void ARadialMenuCharacter::Pressed()
{
	WidgetInteractionComponent->PressPointerKey(EKeys::LeftMouseButton.GetFName());
}

void ARadialMenuCharacter::Released()
{
	WidgetInteractionComponent->ReleasePointerKey(EKeys::LeftMouseButton.GetFName());
}
```

## To enable click events and mouse visibility do the following
```cpp
UFUNCTION(BlueprintCallable)
void MouseVisibility(){
    APlayerController *MyController = GetWorld()->GetFirstPlayerController();

    MyController->bShowMouseCursor = true;
    MyController->bEnableClickEvents = true;
    MyController->bEnableMouseOverEvents = true;
}
```
```cpp
void SetupPlayerInputComponent[...]{
	// Show/Hide Cursor
	PlayerInputComponent->BindAction("MouseVisibility", IE_Pressed, this, &ARadialMenuCharacter::MouseVisibility);
}
```
Finally add `MouseVisibility` named Action Mapping into Project Settings.

**At the end be sure that “UMG” is in the public dependency modules**